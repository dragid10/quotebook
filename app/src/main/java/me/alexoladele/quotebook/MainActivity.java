package me.alexoladele.quotebook;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private Button startButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        startButton = (Button) findViewById(R.id.start_button);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startButton != null) {
                    Toast.makeText(getApplicationContext(), R.string.kanye_toast, Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(), QuotebookActivity.class);
                    startActivity(i);
                }
            }
        });

    }


}


