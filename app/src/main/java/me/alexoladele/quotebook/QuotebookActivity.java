package me.alexoladele.quotebook;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuotebookActivity extends AppCompatActivity {
    private int count = 0;
    private TextView quoteText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotebook);

        quoteText = (TextView) findViewById(R.id.quote);

        // Makes quotes array
        String[] quotes = getResources().getStringArray(R.array.kanye_quotes_array);

        // Put quotes array into arraylist
        final List<String> quoteList = new ArrayList<>(Arrays.asList(quotes));

        if (quoteText != null)
            quoteText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (count >= quoteList.size()) {
                        count = 0;
                    }
                    Quote q = new Quote("");
                    q.setQuote(quoteList.get(count));

                    quoteText.setText(q.getQuote());
                    count++;
                    quoteText.setTextColor(Color.BLUE);
                }
            });
    }
}
