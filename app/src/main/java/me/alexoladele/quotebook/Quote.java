package me.alexoladele.quotebook;

/**
 * Created by dragi on 4/22/2016.
 */
public class Quote {
    public String quote;

    public Quote(String quote) {
        super();

        setQuote(quote);


    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }
}
